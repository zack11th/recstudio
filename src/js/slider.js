import 'owl.carousel';

export default function () {
    $('.owl-carousel').owlCarousel({
        items: 1,
        loop: true,
        nav: true,
        dots: true,
        navText: ["<img src='../../img/butttons/prev.png'>", "<img src='../../img/butttons/next.png'>"]
    })
}