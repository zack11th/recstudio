import 'core-js/features/promise' // подключение полифила для IE

export default function () {
    (new Promise(function (resolve, reject) {
        setTimeout(resolve, 500)
    })).then(() => {
        console.log('promise work')
    })
}