import player from '../templates/blocks/audioPlayer'
import slider from './slider'
import float from './floatMenuAndButtonUp'
import modal from './modal'

window.addEventListener('load', function () {
    float()
    player()
    slider()
    modal()
});


