export default function () {
    let $btns = $('.modal-activated')
    let $modal = $('.modal')
    $btns.on('click', function (e) {
        e.preventDefault()
        $modal.removeClass('is-hidden')
    })
    $(document).mouseup(function (e) {
        if ($modal.has(e.target).length === 0) {
            $modal.addClass('is-hidden')
        }
    })
}