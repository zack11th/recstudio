export default function () {
    let usrAg = navigator.userAgent
    if(usrAg.indexOf('Edge') > -1 || usrAg.indexOf("Trident") > -1) {
        $('.player__volume').addClass('is-hidden')
    }

    player()
}

function player() {
    let song
    let currentAudio
    let volume = 1
    // проверка браузера на возможность воспроизводить тип аудио файла
    let candidate = document.querySelector('.player[data-song]').dataset.song
    candidate = new Audio(candidate + '.mp3')
    let typeAudio = '.ogg'
    if (candidate.canPlayType('audio/mp3') === 'probably') {
        typeAudio = '.mp3'
    }
    // инициализация всех плееров
    let playersList = document.querySelectorAll('.player[data-song]')
    for (let i = 0; i < playersList.length; i++){ // получаем начальные данные об аудио
        playersList[i].path = playersList[i].dataset.song
        playersList[i].songName = playersList[i].dataset.songName
        playersList[i].type = typeAudio
        playersList[i].volume = volume
        playersList[i].mute = false
        playersList[i].querySelector('.player__song-name').innerHTML = playersList[i].songName
        let song = new Audio(playersList[i].path + typeAudio)
        song.addEventListener('loadedmetadata', function () {
            playersList[i].duration = this.duration
            let fulltime = parseInt(playersList[i].duration/60) + ':' + seconds(parseInt(playersList[i].duration%60))
            playersList[i].querySelector('.player__fulltime').innerHTML = fulltime
        })
    }
    // обработчики событий
    $('.player__play-pause').on('click', playPause)
    $('.player__mute').on('click', muteUnmute)
    $('.player__volume').on('input', volumeChange)
    $('.player__range').on('mouseenter', showTime)

    function playPause() { // play & pause
        let player = this.parentNode.parentNode
        if (!!song) {
            if (currentAudio === player) {
                if (song.paused) {
                    $(this).children('i').removeClass('fa-play').addClass('fa-pause')
                    playSong(player)
                } else {
                    $(this).children('i').removeClass('fa-pause').addClass('fa-play')
                    song.pause()
                }
            } else {
                let stop = currentAudio.querySelector('.player__play-pause-icon')
                $(stop).removeClass('fa-pause').addClass('fa-play')
                currentAudio.querySelector('.player__time').innerHTML = '0:00'
                $(currentAudio.querySelector('.player__progress')).css({'width': '0%'})
                $(currentAudio.querySelector('.player__range')).unbind('click')
                song.pause()
                currentAudio = player
                song.src = player.path + player.type
                $(this).children('i').removeClass('fa-play').addClass('fa-pause')
                playSong(player)
            }
        } else {
            currentAudio = player
            song = new Audio(player.path + player.type)
            $(this).children('i').removeClass('fa-play').addClass('fa-pause')
            playSong(player)
        }
    }

    function playSong(player) { // воспроизведение
        song.play()
        song.muted = player.mute
        song.volume = player.volume
        song.addEventListener('timeupdate', function () {
            let curtime = song.currentTime
            let progress = (curtime / player.duration) * 100
            if (currentAudio === player) {
                player.querySelector('.player__time').innerHTML = parseInt(curtime/60) + ':' + seconds(parseInt(curtime%60))
                $(player.querySelector('.player__progress')).css({'width': progress + '%'})
            }
        })
        song.addEventListener('progress', function () {
            let load = song.buffered.length > 0 ? song.buffered.end(0) : 0
            load = (load / player.duration) * 100
            if (currentAudio === player) {
                $(player.querySelector('.player__load')).css({'width': load + '%'})
            }
        })
        song.addEventListener('ended', function () {
            $(player.querySelector('.player__play-pause-icon')).removeClass('fa-pause').addClass('fa-play')
        })
    }

    function muteUnmute() { // mute
        let player = this.parentNode.parentNode
        if (currentAudio === player) {
            if (player.mute === false) {
                player.mute = true
                $(this).children('i').removeClass('fa-volume-up').addClass('fa-volume-off')
                $(player.querySelector('.player__volume')).val(0)

            } else {
                player.mute = false
                $(this).children('i').removeClass('fa-volume-off').addClass('fa-volume-up')
                $(player.querySelector('.player__volume')).val(player.volume * 100)
            }
            song.muted = player.mute
        }
    }

    function volumeChange() { // volume
        let player = this.parentNode.parentNode
        let val = $(this).val()
        volume = val/100
        player.volume = volume
        if (currentAudio === player) {
            song.volume = volume
            if (val === '0') {
                player.mute = true
                $(player.querySelector('.player__mute-icon')).removeClass('fa-volume-up').addClass('fa-volume-off')
            } else if (val > 0) {
                player.mute = false
                if($(player.querySelector('.player__mute-icon')).hasClass('fa-volume-off')) {
                    $(player.querySelector('.player__mute-icon')).removeClass('fa-volume-off').addClass('fa-volume-up')
                }
            }
            song.muted = player.mute
        }
    }

    function showTime() { // тултип времени над полосой прокрутки
        let player = this.parentNode.parentNode.parentNode
        if (currentAudio === player) {
            let offset = $(this).offset()
            let dur = player.duration
            let w = $(this).width()
            $(player.querySelector('.player__setTime')).show()
            $('.player__range').on('mousemove', function (e) {
                let x = e.pageX - offset.left
                let xproc = (x * 100) / w
                let sec = (xproc * dur) / 100
                $(player.querySelector('.player__setTime')).css({'left': x-10})
                $(player.querySelector('.player__setTime')).text(parseInt(sec/60) + ':' + seconds(parseInt(sec%60)))
            })
            $(player.querySelector('.player__range')).on('click', function (e) {
                let x = e.pageX - offset.left
                let xproc = (x * 100) / w
                $(player.querySelector('.player__progress')).css({'width': xproc + '%'})
                song.currentTime = (xproc * dur) / 100
            })
        }
    }
    $('.player__range').on('mouseleave', function () { // убирает туллтип времени над полосой прокрутки
        $('.player__setTime').hide()
    })
}

function seconds(str) { // для корректного отображения секунд
    str = str.toString()
    return str.length < 2 ? '0' + str : str

}