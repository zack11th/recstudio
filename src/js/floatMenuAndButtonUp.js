export default function () {
    let timer
    let btnUpIsOpen = false
    let $btnUp = $('.button-up')
    let $floatMenu = $('.float-menu')
    let winHeight = document.documentElement.clientHeight
    let headerHeight = $('.header').innerHeight() -92
    let docHeight = $(document).height()
    let stopPos = docHeight - winHeight
    let $mobBtn = document.querySelector('.nav__burger')

    onScroll.call(window) // чтобы сразу проверить необходимость отображения элементов

    $(window).on('scroll', function () {
        clearInterval(timer);
        timer = setTimeout(onScroll, 100);
    })
    $mobBtn.addEventListener('click', mobMenu)
    $($btnUp).on('click', function () {
        $('html, body').animate({
            scrollTop: 0
        }, 500)
    })

    function onScroll() {
        let pos = $(this).scrollTop()

        if (!btnUpIsOpen && pos > headerHeight) {
            $btnUp.stop(true).show(500)
            $floatMenu.stop(true).show(500)
            btnUpIsOpen = true
        }
        if (btnUpIsOpen && pos <= headerHeight) {
            btnUpIsOpen = false
            $btnUp.stop(true).hide(500)
            $floatMenu.stop(true).hide(500)
        }
        if (btnUpIsOpen && pos > stopPos) {
            $btnUp.css({
                position: 'absolute',
                bottom: 25
            })
        }
        if (btnUpIsOpen && pos <= stopPos) {
            $btnUp.css({
                position: 'fixed',
                bottom: 25
            })
        }
    }
    function mobMenu(e) {
        e.preventDefault()
        this.classList.toggle('nav__burger--active')
        let $mobMenu = document.querySelector('.nav__menu')
        $mobMenu.classList.toggle('nav__menu--visible')
    }
}

